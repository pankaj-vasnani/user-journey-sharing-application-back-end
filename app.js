const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const placesRouter = require('./routes/places');
const usersRouter = require('./routes/users');

const HttpError = require('./models/http-error');

const app = express();

app.use(bodyParser.json())

app.use('/api/places', placesRouter)
app.use('/api/users', usersRouter)

// Middleware to make sure error is thrown for unknown route
app.use((req, res, next) => {
    const error = new HttpError("Could not find the route", 404);

    throw error;
});

app.use((error, req, res, next) => {
    if(res.headerSent) {
        return next(error);
    }

    res.status(error.code || 500).json({error: error.message || "An unknown error occured"});
})

mongoose
    .connect('mongodb://localhost/user_journey_sharing_system_db', {useUnifiedTopology: true, useNewUrlParser: true})
    .then(() => {
        app.listen(5000);
        console.log("Connected to the database");
    })
    .catch((err) => {
        console.log(err);
    });