const HttpError = require('../models/http-error');
const uuid = require('uuid/v4');
const {validationResult} = require('express-validator');
const getCoordinatesFromAddress = require('../util/location');
const Place = require('../models/place');

const getPlaceById = async (req, res, next) => {
    const id = req.params.id

    try{
        const place = await Place.findById(id)

        res.status(200).json({data: place})
    } catch(err) {
        const error = new HttpError("There is an error in getting the data", 404);

        return next(error);
    }
}

const getAllPlaces = async (req, res, next) => {
    let result
    
    try {
        result = await Place.find()
    } catch(err) {
        const error = new HttpError("There is an error in saving the data", 500);

        return next(error);
    }

    res.status(200).json({data: result})
}


const getPlacesByUserId = async (req, res, next) => {
    const id = req.params.uid

    try {
        const placesData = await Place.find({
            creator: id
        })

        res.status(200).json({data: placesData})
    } catch(err) {
        return next(
            new HttpError("Places not found for the user", 500)
        );
    }
}


const addNewPlace = async  (req, res, next) => {
    const errors = validationResult(req);

    if(! errors.isEmpty()) {
        // res.status(422).json({error: errors.errors});
        console.log(errors);
        throw new HttpError("Invalid input. Please enter the valid data for the place", 422);
    }

    const {name, description, address, creator, coordinates}  = req.body

    // const location = getCoordinatesFromAddress();

    const createdPlace = new Place({
        name,
        description,
        address,
        location: coordinates,
        imageUrl: 'https://media.tacdn.com/media/attractions-splice-spp-674x446/06/74/aa/fc.jpg',
        creator
    });

    let result 
    
    try {
        result = await createdPlace.save()
    } catch(err) {
        const error = new HttpError("There is an error in saving the data", 500);

        return next(error);
    }
    
    res.status(201).json({data: result})
}


const updatePlace = async (req, res, next) => {
    const errors = validationResult(req);

    if(! errors.isEmpty()) {
        throw new HttpError("Invalid input. Please enter the valid data for the place", 422);
    }

    const id = req.params.id
    const {name, description, address, location}  = req.body

    let place

    try {
        place = await Place.findById(id)
    } catch(err) {
        const error = new HttpError("Place not found", 500);

        return next(error);
    }   

    // Update the place
    place.name          = name
    place.description   = description
    place.address       = address
    place.location      = location

    try {
        const result = await place.save();

        res.status(200).json({data: result})
    } catch(err) {
        const error = new HttpError("There is an error in updating the data", 500);

        return next(error);
    }
}


const deletePlace = async (req, res, next) => {
    try {
        place = await Place.findById(req.params.id)

        
    } catch(err) {
        const error = new HttpError("Place not found", 500);

        return next(error);
    }  


    // Delete the place
    try {
        await place.remove();

        res.status(204).json({})
    } catch(err) {
        return next(new HttpError("There was an error in deleting the place", 500))
    }
}


exports.getAllPlaces = getAllPlaces
exports.getPlaceById = getPlaceById
exports.getPlacesByUserId = getPlacesByUserId
exports.addNewPlace = addNewPlace
exports.updatePlace = updatePlace
exports.deletePlace = deletePlace