const HttpError = require('../models/http-error');
const uuid = require('uuid/v4');
const {validationResult} = require('express-validator')
const User = require('../models/user');


const getAllUsers = async (req, res, next) => {
    try {
        const user = await User.find({}, '-password');
        res.status(200).json({data: user});
    } catch(err) {
        return next(new HttpError("There is an error in getting the data", 500));
    }
}


const addNewUser = async (req, res, next) => {
    const errors = validationResult(req);

    if(! errors.isEmpty()) {
         return next(new HttpError("Invalid input. Please enter the valid data for the user", 422));
    }

    const {name, image, email, password}  = req.body

    let alreadyHasUser;

    try {
         alreadyHasUser = await User.findOne(
            {
                email: req.body.email
            }
        );
    } catch(err) {
        return next(new HttpError("There is an error in saving the data", 500));
    }

    if(alreadyHasUser) {
        return next(HttpError("User already created", 400));
    }

    const createdUser = new User({
        name,
        image,
        places: [],
        email,
        password
    });

    try {
        const result = await createdUser.save();

        res.status(201).json({data: createdUser});
    } catch(err) {
        return next(new HttpError("There is an error in saving the data", 500));
    }

    
}


const login = async (req, res, next) => {
    const errors = validationResult(req);

    if(! errors.isEmpty()) {
        return next(new HttpError("Invalid login credentials.", 422));
    }

    const {email, password} = req.body

    try {
        const identifiedUser = await User.findOne({
            email: email
        })

        if(! identifiedUser || identifiedUser.password !== password) {
            return next(new HttpError("Credentials does not match", 401));
        }

        res.status(200).json({data: identifiedUser})
    } catch(err) {
        return next(new HttpError("There is an error in getting the data", 500));
    }
}


exports.getAllUsers = getAllUsers
exports.addNewUser  = addNewUser
exports.login  = login 