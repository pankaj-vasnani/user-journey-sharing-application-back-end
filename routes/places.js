const express = require('express');

const router = express.Router();

const placesControllers = require('../controllers/places-controller');

const { check } = require('express-validator')

router.get('/user/:uid', placesControllers.getPlacesByUserId)

router.get('/:id', placesControllers.getPlaceById)

router.get('/', placesControllers.getAllPlaces);

router.post('/', 
 [
    check('name').not().isEmpty(),
    check('description').isLength({min: 5}),
    check('address').not().isEmpty()
 ],
  placesControllers.addNewPlace)

router.put('/:id', 
[
    check('name').not().isEmpty(),
    check('description').isLength({min: 5}),
    check('address').not().isEmpty()
],
placesControllers.updatePlace)

router.delete('/:id', placesControllers.deletePlace)

module.exports = router;