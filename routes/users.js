const express = require('express');

const router = express.Router();

const usersControllers = require('../controllers/users-controller');

const { check } = require('express-validator');

router.get('/', usersControllers.getAllUsers)

router.post('/', 
[
    check('name').not().isEmpty(),
    check('email').not().isEmpty(),
    check('password').not().isEmpty().isLength({min: 6})
 ],
usersControllers.addNewUser)

router.post('/login', 
[
    check('email').not().isEmpty(),
    check('password').not().isEmpty().isLength({min: 6})
 ],
usersControllers.login);

module.exports = router;